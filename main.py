from flask import Flask, render_template, request, flash, session
import netdev
import asyncio
import jtextfsm as textfsm

app = Flask(__name__)
app.secret_key = '2b4260e85bce4bf9a5c74d2733578251'
exceptions = (netdev.exceptions.DisconnectError,
              netdev.exceptions.TimeoutError)

# >>>>> without the use of jtextfsm
# def extract_ips_from_out(out):
#     lines = out.split('\n')
#     del lines[0]  #delete headers
#     ips = [line.split()[1] for line in lines]
#     return ips


def extract_ips_from_out(out):
    with open('textfsm/arp_vrf_MGMT_VRF.textfsm') as vrf_template:
        re_table = textfsm.TextFSM(vrf_template)
    out_parsed = re_table.ParseText(out)
    ips = []
    for item in out_parsed:
        ips += item
    return ips


def extract_int_topology_from_out(out):
    with open('textfsm/int_desc.textfsm') as int_template:
        re_table = textfsm.TextFSM(int_template)
    out_parsed = re_table.ParseText(out)
    int_topology = {}
    for item in out_parsed:
        interface, status, hostname = item[0], item[1], item[2]
        int_topology[interface] = {
            'status': status,
            'to_hostname': hostname
        }
    return int_topology


def extract_cdp_topology_from_out(out):
    with open('textfsm/cdp_nei_det.textfsm') as cdp_template:
        re_table = textfsm.TextFSM(cdp_template)
    out_parsed = re_table.ParseText(out)
    out_grouped_by_nei = [out_parsed[i*4:(i+1)*4]
                          for i in range(len(out_parsed) // 4)]
    cdp_topology = {}
    for item in out_grouped_by_nei:
        interface_label, interface_num, nei_hostname, nei_ip = item[
            2][1], item[2][2], item[0][0], item[1][3]

        if interface_label == 'GigabitEthernet':
            interface = 'Gi' + interface_num
        elif interface_label == 'TenGigabitEthernet':
            interface = 'Te' + interface_num
        else:
            continue

        # map ip to its hostname
        session['ip_hostname'][nei_ip] = nei_hostname

        cdp_topology[interface] = {
            'nei_hostname': nei_hostname,
            'nei_ip': nei_ip
        }
    return cdp_topology


def extract_light_levels_from_out(out):
    with open('textfsm/int_trans.textfsm') as trans_template:
        re_table = textfsm.TextFSM(trans_template)
    out_parsed = re_table.ParseText(out)
    light_levels = {}
    for item in out_parsed:
        interface, tx, rx = item[0], item[1], item[2]
        light_levels[interface] = {
            'tx': tx,
            'rx': rx
        }
    return light_levels


async def get_all_ips(md01_param):
    async with netdev.create(**md01_param) as md01:
        out = await md01.send_command('sh arp vrf MGMT_VRF')

    # save ips into session
    session['ips'] = extract_ips_from_out(out)


# check protocol status, CDP neighbor connections, and light levels of a device
async def check_devi(devi_param):
    try:
        async with netdev.create(**devi_param) as devi:

            physical_errors = []
            physical_success = []
            light_level_errors = []
            light_level_success = []

            out = await devi.send_command('show int desc')
            int_topology = extract_int_topology_from_out(out)
            # print('-----------------int_topology---------------------')
            # print(device_param['host'])
            # print(int_topology)
            # print()

            out = await devi.send_command('show cdp nei det')
            cdp_topology = extract_cdp_topology_from_out(out)
            # print('-----------------cdp_topology---------------------')
            # print(devi_param['host'])
            # print(cdp_topology)
            # print()

            # check protocol status - errors if portocol status is down
            for interface in int_topology:
                if int_topology[interface]['status'] != 'up':
                    physical_errors.append(
                        f'{interface} status is down')
                else:
                    physical_success.append(
                        f'{interface} status checked')

                # check connections - errors if CDP neighbor hostname does not match interface description)
                if interface not in cdp_topology:
                    physical_errors.append(f'{interface} should connect to: {int_topology[interface]["to_hostname"]}. \
                        Currently connected to: none')
                elif int_topology[interface]['to_hostname'] != cdp_topology[interface]['nei_hostname']:
                    physical_errors.append(f'{interface} should connect to: {int_topology[interface]["to_hostname"]}. \
                        Currently connected to: {cdp_topology[interface]["nei_hostname"]}')
                else:
                    physical_success.append(
                        f'{interface} - {int_topology[interface]["to_hostname"]} CDP neighbor checked')

            # check light levels - errors if Rx or Tx light level is below -13 or above -4
            out = await devi.send_command('show int trans')
            light_levels = extract_light_levels_from_out(out)
            for interface in light_levels:
                if float(light_levels[interface]['tx']) <= -13.0 or float(light_levels[interface]['rx']) <= -13.0:
                    light_level_errors.append(
                        f'Low light on interface {interface} - Tx: {light_levels[interface]["tx"]}, Rx: {light_levels[interface]["rx"]}')
                elif float(light_levels[interface]['tx']) >= -4 or float(light_levels[interface]['rx']) >= -4:
                    light_level_errors.append(
                        f'Light over threshold on interface {interface} - Tx: {light_levels[interface]["tx"]}, Rx: {light_levels[interface]["rx"]}')
                else:
                    light_level_success.append(
                        f'{interface} port light level checked')

            return [devi_param['host'], physical_errors, physical_success, light_level_errors, light_level_success]
    except Exception as e:
        return [devi_param['host'], [str(e)], [], [], []]



@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        username = request.form.get('uname')
        password = request.form.get('pwd')
        device_type = request.form.get('dtype')
        md01_ip_address = request.form.get('ip')
        if not username or not password or not device_type or not md01_ip_address:
            flash('Please enter all fields', 'required')
            return render_template('index.html')
        else:
            md01 = {
                'username': username,
                'password': password,
                'device_type': device_type,
                'host': md01_ip_address
            }

            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)

            try:
                loop.run_until_complete(get_all_ips(md01))
            except exceptions as e:
                flash(str(e), 'error')
                return render_template('index.html')
            
            # save a mapping between ip and hostname in session   
            session['ip_hostname'] = {}
            session['ip_hostname'][f'{md01_ip_address}'] = 'MD01'

            # check each device one-by-one
            tasks = []
            for ip in session['ips']:
                device = {
                    'username': username,
                    'password': password,
                    'device_type': device_type,
                    'host': ip
                }
                tasks += [check_devi(device)]
            results = loop.run_until_complete(asyncio.gather(*tasks))

            for result in results:
                if result[0] in session['ip_hostname']:
                    result[0] = session['ip_hostname'][result[0]] + ' - ' + result[0]
            return render_template('mallcheck_report.html', results=results)
    return render_template('index.html')


if __name__ == '__main__':
    app.run(debug=True, port=5001)
