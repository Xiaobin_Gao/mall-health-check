# Grid Health Check

> Grid Health Check diagnoses all the MDFs and the IDFs that have been installed and booted in a mall. Physical connections, fiber light levels, and power supplies are the targets to be checked for every detected MDF/IDF.

## Run in Local Development

**> *Install Python3.6+ & virtualenv***

**> *Create & enter an virtual environment***

**> *Install dependencies***
  
    pip install -r requirements.txt

**> *Set environment variables(e.g. cmd)***
  
    set AAA_UN=x            # Username of a device management account   
    set AAA_PW=x            # Password

**> *Run the app***

    python main.py

## API Call

#### Check a mall with the IP address of a MD01 &#x25BE;

    POST: /mallcheck/

###### REQUEST PARAMETERS
*Content-Type: application/json*

    md01_ip_address         The IP address of MDF01 in a mall

###### SAMPLE RESPONSES

    # When the md01_ip_address is not a MDF01
    {"Error": "Host 198.19.138.204 Not-MD01 Error"}

    # When the md01_ip_address is inaccessible/the Granite MPLS VPN is off
    {"Error": "Host 198.19.138.193 Timeout Error"}

    # The successful output, which has the diagnoses of all detected devices of a mall in their own list
    # List pattern: [host, errors of physical connections/exception during checking, success of physical connections, 
                           errors of light levels, success of light levels, 
                           errors of power supplies, success of power supplies]
    {"Success": [
        ["MD01 - 198.19.138.193", [], [
        "Gi0/0/10 status checked",
        "Gi0/0/10 - MD02 CDP neighbor checked",
        "Gi0/0/11 status checked",
        "Gi0/0/11 - MD02 CDP neighbor checked"
        ], [], [
        "Gi0/0/0 port light level checked",
        "Gi0/0/10 port light level checked",
        "Gi0/0/11 port light level checked"
        ], [], [
        "P0 slot power supply checked",
        "P1 slot power supply checked"
        ]
     ],...,
        ["ID00 - 198.19.138.203", [], [
        "Gi0/1 status checked",
        "Gi0/1 - MD02 CDP neighbor checked",
        "Gi0/2 status checked",
        "Gi0/2 - ID04 CDP neighbor checked"
        ], [], [
        "Gi0/1 port light level checked",
        "Gi0/2 port light level checked"
        ], [], []
     ],...
    ]}
